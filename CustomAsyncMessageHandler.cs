﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client.Core.DependencyInjection;

namespace Examples.ConsumerConsole
{
    public class CustomAsyncMessageHandler : IAsyncMessageHandler
    {
        readonly ILogger<CustomAsyncMessageHandler> _logger;

        public CustomAsyncMessageHandler(ILogger<CustomAsyncMessageHandler> logger)
        {
            _logger = logger;
        }

        public async Task Handle(string message, string routingKey)
        {
            // Do whatever you want asynchronously!
            _logger.LogInformation("Merry Christmas!");
        }
    }
}